package gr.ntua.mongo.launcher;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class JettyServer {
    private static final Logger Log = LoggerFactory.getLogger(JettyServer.class);
    private Server server;
    private int port;
    
    public JettyServer(int port) {
        this.port = port;
    }
    
    
    
    public void connect(String warPath) {
        this.server = new Server(this.port);
        
         Log.info("Server is setting its connector");
         
         AppContextBuilder contextBuilder = null;
         if((warPath==null)||(warPath.equals("")))
             throw new IllegalArgumentException("war path cannot be null");
         else
             contextBuilder = new AppContextBuilder(warPath);
         
         WebAppContext webAppContext = contextBuilder.buildWebAppContext(server);
         
         
         
         HandlerList handlers = new HandlerList();
         handlers.setHandlers(new Handler[] { webAppContext } );
         server.setHandler(handlers);   
         
    }
    
    public void connect() {
         connect(null);     
     }
    
    
    
    public void start() throws Exception {
        if(this.server!=null) {
            Log.info("Server is starting");
            this.server.start();
            this.server.join();            
        }
    }
    
    public String getState() {
        return this.server.getState();
    }
    
    public void stop() throws Exception {
        if(this.server!=null) {
            Log.info("Server is stoping");
            this.server.stop();
            this.server.join();
        }        
    }
    
    public boolean isStarted() {
        if(this.server!=null)
            return this.server.isStarted();
        return false;
    }
    
    public boolean isFailed() throws LauncherException {
        if(this.server!=null)
            return this.server.isFailed();
        throw new LauncherException("server has not been initialized yet", null);
    }
    
    public boolean isRunning() {
        if(this.server!=null)
            return this.server.isRunning();
        return false;
    }
    
    public boolean isStarting() {
        if(this.server!=null)
            return this.server.isStarting();
        return false;
    }
    
    public boolean isStopped() throws LauncherException {
        if(this.server!=null)
            return this.server.isStopped();
        throw new LauncherException("server has not been initialized yet", null);
    }
    
    public boolean isStopping() throws LauncherException {
        if(this.server!=null)
            return this.server.isStopping();
        throw new LauncherException("server has not been initialized yet", null);
    }
}
