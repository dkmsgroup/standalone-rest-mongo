package gr.ntua.mongo.launcher;

import java.util.concurrent.ExecutorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class LauncherShutDownHook extends Thread {
    private static final Logger Log = LoggerFactory.getLogger(LauncherShutDownHook.class);
    private final MongoAppLauncher appLauncher;
    private final ExecutorService executorService;
    
    public LauncherShutDownHook(MongoAppLauncher appLauncher, ExecutorService executorService) {
        this.appLauncher = appLauncher;
        this.executorService = executorService;
    }
    
    @Override
    public void run() {
        Log.info("Shutdown hook is triggered");
        
        if((this.executorService!=null)&&(!this.executorService.isShutdown()))
                this.executorService.shutdownNow();
    
        if(appLauncher.getJettyserver()!=null) {
            try {
                if(!appLauncher.getJettyserver().isStopped())
                    try {
                    appLauncher.getJettyserver().stop();
                } catch (Exception ex) {
                    Log.error(ex.getClass().getName() + ". " + ex.getMessage());
                }
            } catch (LauncherException ex) {
                Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            }
        }
    }
}
