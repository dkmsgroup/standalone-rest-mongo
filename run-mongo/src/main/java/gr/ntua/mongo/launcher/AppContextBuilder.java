package gr.ntua.mongo.launcher;

import java.io.File;
import java.nio.file.Files;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author idezol
 */
public class AppContextBuilder {
    
    private static final Logger Log = LoggerFactory.getLogger(AppContextBuilder.class);
    private WebAppContext webAppContext;
    private final String warPath;

    
    public AppContextBuilder(String warPath) {
        this.warPath = warPath;
    }
  
    public WebAppContext buildWebAppContext(Server server) {
        
        webAppContext = new WebAppContext();
        webAppContext.setServer(server);
        webAppContext.setContextPath("/");
        Log.info("Context Path is set to '/'");

        Log.info("trying to deploy war file from " + warPath);
        if(Files.exists(new File(warPath).toPath())) {
            Log.info("war deployment file found");
        } else {
            Log.warn("Could not find the war deployment file");
        }
        
        
        
        webAppContext.setWar(warPath);
        //webAppContext.setWar(location.toExternalForm());
        
        return webAppContext;
    }
    
}
