package gr.ntua.mongo.launcher;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class App  {
    private static final Logger Log = LoggerFactory.getLogger(App.class);
    private static final String WAR_PATH = "web-mongo.war";
    private static final int PORT = 8089;
    //private static final String WAR_PATH = "C:\\Users\\idezol-user\\Documents\\NetBeansProjects\\rest-mongo\\run-mongo\\web-mongo.war";
    private final MongoAppLauncher appLauncher;
    private final ExecutorService executorService;
    
    private App() {
        this.appLauncher = new MongoAppLauncher(WAR_PATH, PORT);
        this.executorService = Executors.newSingleThreadExecutor();
    }
    
    public static void main( String[] args ) {
        App app = new App();
        app.launch();
    }
    
    public void launch() {
        Log.debug("Adding shutdown hook");
        Runtime runtime = Runtime.getRuntime();
        runtime.addShutdownHook(new LauncherShutDownHook(appLauncher, executorService));
        
        executorService.execute(appLauncher);
    }
}
