package gr.ntua.mongo.launcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class MongoAppLauncher implements Runnable {
    
    private static final Logger Log = LoggerFactory.getLogger(MongoAppLauncher.class);
    JettyServer jettyserver;
    String warPath;
    int port;
    
    public MongoAppLauncher(String warPath, int port) {
        this.warPath = warPath;
        this.port = port;
        this.jettyserver = new JettyServer(port);
    }
    
    @Override
    public void run() {
        
        if(this.jettyserver==null)
            this.jettyserver = new JettyServer(port);
        
        jettyserver.connect(warPath);
        try {
            jettyserver.start();
        } catch (InterruptedException ex) {
            Log.info("Embedded server will now close");
        } catch (Exception ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
        } finally {
            try {
                if((this.jettyserver!=null)&&(!this.jettyserver.isStopped())) {
                    try {
                        this.jettyserver.stop();
                    } catch (Exception ex) {
                        Log.error(ex.getClass().getName() + ". " + ex.getMessage());
                    }
                }
            } catch (LauncherException ex) {
                Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            }
        }
    }

    public JettyServer getJettyserver() {
        return jettyserver;
    }
    
    
}
