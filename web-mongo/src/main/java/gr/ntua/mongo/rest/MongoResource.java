package gr.ntua.mongo.rest;

import gr.ntua.mongo.dba.MongoResourceImpl;
import gr.ntua.mongo.dba.exceptions.MongoOperationException;
import gr.ntua.mongo.dba.exceptions.MongoRestException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST Web Service
 *
 * @author idezol
 */
@Path("mongo")
public class MongoResource {
    private static final Logger Log = LoggerFactory.getLogger(MongoResource.class);

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of MongoResource
     */
    public MongoResource() {
    }
    
    @GET
    @Path("/{database}/{collection}/query")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response queryDocument(
            @PathParam("database") String database,
            @PathParam("collection") String collection,
            String input) {
        
        Log.info("query method has been invoked");
        
        try {
            _checkEmptyParam(database);
            _checkEmptyParam(collection);
        } catch(MongoRestException ex) {
            return MessageHandling.buildErrorResponse("Database and collection names must not be null", Response.Status.BAD_REQUEST);
        }
        
        MongoResourceImpl mongoResourceImpl = new MongoResourceImpl(database, collection);
        String result;
        try {
            result = mongoResourceImpl.query(input);
        } catch(MongoOperationException ex) {
            return MessageHandling.buildErrorResponse(ex);
        }
        
        return Response.ok(result).build();
    }

    @GET
    @Path("/{database}/{collection}/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDocumentQueryByKey(
            @PathParam("database") String database,
            @PathParam("collection") String collection,
            @QueryParam("key") String key,
            @QueryParam("value") String value) {
        
        Log.info("QueryByKey method has been invoked");
        try {
            _checkEmptyParam(database);
            _checkEmptyParam(collection);
            _checkEmptyParam(key);
            _checkEmptyParam(value);
        } catch(MongoRestException ex) {
            return MessageHandling.buildErrorResponse("Parameters must not be null", Response.Status.BAD_REQUEST);
        }
        
        MongoResourceImpl mongoResourceImpl = new MongoResourceImpl(database, collection);
        String result;
        try {
            result = mongoResourceImpl.queryByKey(key, value);
        } catch(MongoOperationException ex) {
            return MessageHandling.buildErrorResponse(ex);
        }
        
        return Response.ok(result).build();
    }
            
    
    @GET
    @Path("/{database}/{collection}/{objectID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDocumentById(
            @PathParam("database") String database,
            @PathParam("collection") String collection,
            @PathParam("objectID") String objString) {
        
        Log.info("GetByID method has been invoked");
        try {
            _checkEmptyParam(database);
            _checkEmptyParam(collection);
            _checkEmptyParam(objString);
        } catch(MongoRestException ex) {
            return MessageHandling.buildErrorResponse("Parameters must not be null", Response.Status.BAD_REQUEST);
        }
        
        MongoResourceImpl mongoResourceImpl = new MongoResourceImpl(database, collection);
        String result;
        try {
            result = mongoResourceImpl.findByID(objString);
        } catch(MongoOperationException ex) {
            return MessageHandling.buildErrorResponse(ex);
        }
        
        return Response.ok(result).build();
    }
    
    @POST
    @Path("/{database}/{collection}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertDocument(
            @PathParam("database") String database,
            @PathParam("collection") String collection,
            String input) {
        
        Log.info("Insert method has been invoked");
        
        try {
            _checkEmptyParam(database);
            _checkEmptyParam(collection);
        } catch(MongoRestException ex) {
            return MessageHandling.buildErrorResponse("Database and collection names must not be null", Response.Status.BAD_REQUEST);
        }
        
        MongoResourceImpl mongoResourceImpl = new MongoResourceImpl(database, collection);
        String result;
        try {
            result = mongoResourceImpl.insert(input);
        } catch(MongoOperationException ex) {
            return MessageHandling.buildErrorResponse(ex);
        }
        
        return Response.ok(result).build();
    }
    
    private void _checkEmptyParam(String param) throws MongoRestException {
        if((param==null)||(param.equals("")))
            throw new MongoRestException("Param cannot be empty");
    }
    
    
} 
