package gr.ntua.mongo.rest;

import gr.ntua.mongo.dba.MongoAccess;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class InitRest implements ServletContextListener {
    private static final Logger Log = LoggerFactory.getLogger(InitRest.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        
        String hostname = sce.getServletContext().getInitParameter("mongo-host");
        String portStr = sce.getServletContext().getInitParameter("mongo-port");
        
        if((hostname==null)||(portStr==null)
                || (hostname.equals("")) || (portStr.equals(""))) {
            MongoAccess.getInstance();
            Log.info("MongoClient has been initialized on default localhost:27017");
            return;
        }
        
        int port;
        try {
            port = Integer.valueOf(portStr);
        } catch(NumberFormatException ex) {
            Log.error("Port {} is not in a valid format", portStr);
            throw ex;
        }
        
        MongoAccess.newInstance(hostname, port);
        Log.info("MongoClient has been initialized to {}:{}", hostname, port);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        MongoAccess.getInstance().close();
        Log.info("MongoClient has been destroyed");
    }
        

    
}
