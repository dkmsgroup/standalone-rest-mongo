package gr.ntua.mongo.rest;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author idezol
 */
public class MessageHandling {
    private MessageHandling() {} 
    
    public static String getJsonMessage(String message) {       
        return "{\"message\": \"" + message + "\"}";
    }
    
    public static String getJsonMessage(Exception ex) {       
        return "{\"message\": \"" + ex.getClass().getName() + ": " +  ex.getMessage() + "\"}";
    }
    
    public static Response buildErrorResponse(Exception ex) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(getJsonMessage(ex))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
    }
    
    public static  Response buildErrorResponse(String message, Response.Status statusResponse) {
        return Response.status(statusResponse)
                    .entity(getJsonMessage(message))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
    }
}
