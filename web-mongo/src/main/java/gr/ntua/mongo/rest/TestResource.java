package gr.ntua.mongo.rest;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST Web Service
 *
 * @author idezol
 */
@Path("test")
public class TestResource {
    private static final Logger Log = LoggerFactory.getLogger(TestResource.class);

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of TestResource
     */
    public TestResource() {
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response helloWorld() {
        Log.info("Hello world rest method has been invoked");
        String result = "<b>hello world</b>";
        return Response.ok(result).build();
    }
}
