package gr.ntua.mongo.dba.exceptions;

/**
 *
 * @author idezol
 */
public class MongoOperationException extends Exception {
    public MongoOperationException(String message) {
        super(message);
    }
    
    public MongoOperationException(String message, Throwable cause) {
        super(message, cause);
    }
}
