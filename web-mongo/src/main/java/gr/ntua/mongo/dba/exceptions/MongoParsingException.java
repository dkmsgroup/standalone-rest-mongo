package gr.ntua.mongo.dba.exceptions;

/**
 *
 * @author idezol
 */
public class MongoParsingException extends Exception{
    public MongoParsingException(String message) {
        super(message);
    }
    
    public MongoParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
