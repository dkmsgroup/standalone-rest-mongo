package gr.ntua.mongo.dba.utils;

import gr.ntua.mongo.dba.exceptions.MongoParsingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class ParseUtil {
    private static final Logger Log = LoggerFactory.getLogger(ParseUtil.class);
    
    private ParseUtil() { }
    
    public static Document getQueryByKeyValue(String key, String value) throws MongoParsingException {
        if(key==null)
            throw new MongoParsingException("key cannot be null");
        
        
        Document document = new Document(key, value);
        return document;
    }
    
    public static Document getQueryByID(String input) throws MongoParsingException {
        if(input==null)
            throw new MongoParsingException("id cannot be null");
        
        ObjectId objectId;
        try {
            objectId = new ObjectId(input);
        } catch(Exception ex) {
            throw new MongoParsingException("id cannot be parsed to null object");
        }
        
        Document document = new Document("_id", objectId);
        return document;
    }
     
    public static Document toMongoDocument(String input) throws MongoParsingException {
        if(input==null)
            return new Document();
        
        Document document;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Map<String, Object> map = objectMapper.readValue(input, HashMap.class);
            document = new Document(map);
        } catch(IOException ex) { 
            Log.error("Error parsing input string to valid Document object. Input: {}. {}:{}", input, ex.getClass().getName(), ex.getMessage());
            throw new MongoParsingException("Error parsing input to Document object", ex);
        }
        
        return document;
    }
    
    public static String documentToString(List<Document> list) throws MongoParsingException {
        List<String> result = new ArrayList<>();
        for(Document doc : list)
            result.add(doc.toJson());
        
        return serialize(result);
    }
    
    public static String serialize(Object object) throws MongoParsingException {
        ObjectMapper mapper = new ObjectMapper();
        String json;
        mapper.setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
        try {
            json = mapper.writeValueAsString(object);
        } catch (JsonParseException ex) {
            String message = "JsonGenerationException. Cannot parse the user's input json file. " + ex.getMessage();
            throw new MongoParsingException(message, ex);
        } catch (JsonMappingException ex) {
            String message = "JsonMappingException. Cannot map values to the user's input json file. " + ex.getMessage();
            throw new MongoParsingException(message, ex);
        } catch (IOException ex) {
            String message = "IOException. Cannot read user' input json file. " + ex.getMessage();
            throw new MongoParsingException(message, ex);
        }
        return json;
    }
    
    public static String documentToString(Document document) {
        if(document==null)
            return "";
        
        return document.toJson();
    }
} 
 