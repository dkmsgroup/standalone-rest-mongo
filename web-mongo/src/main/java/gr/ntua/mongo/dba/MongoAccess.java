package gr.ntua.mongo.dba;

import com.mongodb.MongoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class MongoAccess {
    private static final Logger Log = LoggerFactory.getLogger(MongoAccess.class);
    private final MongoClient mongoClient;
    private static MongoAccess instance;
    
    private MongoAccess() {
        mongoClient = new MongoClient();
    }
    
    private MongoAccess(String host, int port) {
        mongoClient = new MongoClient(host, port);
    }
    
    public static MongoAccess newInstance(String host, int port) {
        while(instance==null) {
            synchronized(MongoAccess.class) {
                if(instance==null)
                    instance = new MongoAccess(host, port);
            }
        }
        return instance;
    }
    
    public static MongoAccess getInstance() {
        while(instance==null) {
            synchronized(MongoAccess.class) {
                if(instance==null)
                    instance = new MongoAccess();
            }
        }
        return instance;
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }
    
    
    public void close() {
        synchronized(MongoAccess.class) {
            if(MongoAccess.instance.mongoClient!=null) {
                MongoAccess.instance.mongoClient.close();
                MongoAccess.instance = null;
            }
        }
    }
}
