package gr.ntua.mongo.dba;

import gr.ntua.mongo.dba.exceptions.MongoParsingException;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import gr.ntua.mongo.dba.exceptions.MongoOperationException;
import gr.ntua.mongo.dba.utils.ParseUtil;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class MongoResourceImpl {
    private static final Logger Log = LoggerFactory.getLogger(MongoResourceImpl.class);
    private final MongoClient mongoClient;
    private final MongoDatabase mongoDatabase;
    private final MongoCollection<Document> mongoCollection;
    
    public MongoResourceImpl(String databaseName, String collectionName) {
        this.mongoClient = MongoAccess.getInstance().getMongoClient();
        this.mongoDatabase = this.mongoClient.getDatabase(databaseName);
        this.mongoCollection = this.mongoDatabase.getCollection(collectionName);
    }
    
    public String query(String input) throws MongoOperationException {
        _CheckIfValid();
        
        Document queryDocument;
        try {
            queryDocument = ParseUtil.toMongoDocument(input);
        } catch (MongoParsingException ex) {
            throw new MongoOperationException(ex.getMessage(), ex);
        }
        
         MongoCursor<Document> cursor;
        try {
            cursor = this.mongoCollection.find(queryDocument).iterator();
        } catch(Exception ex) {
            Log.error("Error getting document with query: {}/{}. {}:{}", input, ex.getClass().getName(), ex.getMessage());
            throw new MongoOperationException(ex.getMessage(), ex);
        }
        
        List<Document> results = new ArrayList<>();
        while(cursor.hasNext())
            results.add(cursor.next());
        
        String str;
        try {
            str = ParseUtil.documentToString(results);
        } catch (MongoParsingException ex) {
            throw new MongoOperationException(ex.getMessage(), ex);
        }
        
        return str;
    }
    
    public String queryByKey(String key, String value) throws MongoOperationException {
        _CheckIfValid();
        
        Document queryDocument;
        try {
            queryDocument = ParseUtil.getQueryByKeyValue(key, value);
        } catch (MongoParsingException ex) {
            throw new MongoOperationException(ex.getMessage(), ex);
        }
        
        MongoCursor<Document> cursor;
        try {
            cursor = this.mongoCollection.find(queryDocument).iterator();
        } catch(Exception ex) {
            Log.error("Error getting document with key/value: {}/{}. {}:{}", key, value, ex.getClass().getName(), ex.getMessage());
            throw new MongoOperationException(ex.getMessage(), ex);
        }
        
        List<Document> results = new ArrayList<>();
        while(cursor.hasNext())
            results.add(cursor.next());
        
        String str;
        try {
            str = ParseUtil.documentToString(results);
        } catch (MongoParsingException ex) {
            throw new MongoOperationException(ex.getMessage(), ex);
        }
        
        return str;
    }
    
    public String findByID(String objIDStr) throws MongoOperationException {
        _CheckIfValid();
        
        Document queryDocument;
        try {
            queryDocument = ParseUtil.getQueryByID(objIDStr);
        } catch (MongoParsingException ex) {
            throw new MongoOperationException(ex.getMessage(), ex);
        }
        
        MongoCursor<Document> cursor;
        try {
            cursor = this.mongoCollection.find(queryDocument).iterator();
        } catch(Exception ex) {
            Log.error("Error getting document with id: {}. {}:{}", objIDStr, ex.getClass().getName(), ex.getMessage());
            throw new MongoOperationException(ex.getMessage(), ex);
        }
        
        Document result = cursor.next();
        return ParseUtil.documentToString(result);
    }
    
    public String insert(String input) throws MongoOperationException {
        _CheckIfValid();
        
        Document insertDocument;
        try {
            insertDocument = ParseUtil.toMongoDocument(input);
        } catch (MongoParsingException ex) {
            throw new MongoOperationException(ex.getMessage(), ex);
        }
        
        try {
            this.mongoCollection.insertOne(insertDocument);
        } catch(Exception ex) {
            Log.error("Error inserting document: {}. {}:{}", insertDocument.toJson(), ex.getClass().getName(), ex.getMessage());
            throw new MongoOperationException(ex.getMessage(), ex);
        }
        
        return ParseUtil.documentToString(insertDocument);
    }
    
    private void _CheckIfValid() throws MongoOperationException {
        if(this.mongoCollection==null) {
            IllegalAccessException ex = new  IllegalAccessException("Mongo Collection is not properly initialized");
            throw new MongoOperationException(ex.getMessage(), ex);
        }
    }
}
