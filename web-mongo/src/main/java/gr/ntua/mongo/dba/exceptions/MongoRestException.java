package gr.ntua.mongo.dba.exceptions;

/**
 *
 * @author idezol
 */
public class MongoRestException extends Exception {
    public MongoRestException(String message) {
        super(message);
    }
    
    public MongoRestException(String message, Throwable cause) {
        super(message, cause);
    }
}
